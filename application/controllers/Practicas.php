<?php

class Practicas extends CI_Controller
{

  function __construct()
  {
    // code...
    parent:: __construct();
    //cargamos el modelo que llama a la base de datos
    $this->load->model('practica');
  }
  public function index(){
    $data["listado"]=$this->practica->obtenerTodos();
	  $this->load->view('header');
    $this->load->view('practicas/index',$data);
    $this->load->view('footer');
    // debemos crear en views folder practicas y el inf¿dex
  }

  public function nuevo(){
    $data["listado"]=$this->practica->obtenerTodos();
	  $this->load->view('header');
    $this->load->view('practicas/nuevo',$data);
    $this->load->view('footer');
    // debemos crear en views folder practicas y el inf¿dex
  }

  public function guardar(){
    $datos=array(
      "cedula_br"=>$this->input->post("cedula_br"),
      "nombre_br"=>$this->input->post("nombre_br"),
      "apellido_br"=>$this->input->post("apellido_br"),
      "celular_br"=>$this->input->post("celular_br"),
	    "email_br"=>$this->input->post("email_br"),
      "latitud_br"=>$this->input->post("latitud_br"),
	    "latitud2_br"=>$this->input->post("latitud2_br"),
	    "latitud3_br"=>$this->input->post("latitud3_br"),
	    "latitud4_br"=>$this->input->post("latitud4_br"),
      "longitud_br"=>$this->input->post("longitud_br"),
	    "longitud2_br"=>$this->input->post("longitud2_br"),
	    "longitud3_br"=>$this->input->post("longitud3_br"),
	    "longitud4_br"=>$this->input->post("longitud4_br")
    );
    if($this->practica->insertar($datos)){
      redirect("practicas/nuevo");
    }else{
      echo "EROOR AL INSERTAR";
    }
  }
  public function insertarLT(){
	$datos=array(
		"latitud_br"=>$this->input->post("latitud_br"),
	  "latitud2_br"=>$this->input->post("latitud2_br"),
	  "latitud3_br"=>$this->input->post("latitud3_br"),
	  "latitud4_br"=>$this->input->post("latitud4_br"),
    "longitud_br"=>$this->input->post("longitud_br"),
	  "longitud2_br"=>$this->input->post("longitud2_br"),
	  "longitud3_br"=>$this->input->post("longitud3_br"),
	  "longitud4_br"=>$this->input->post("longitud4_br")
		);
    if($this->practica->insertar($datos)){

     echo"exitosa de Insercion";

    }else{
     echo"Error de Insercion";

    }

}
}


