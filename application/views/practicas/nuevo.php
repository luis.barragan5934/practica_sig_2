<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo</title>

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <!-- bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- importar API google maps -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXV6fJ3AmkKcvYoxYWcAyQ5Y1ddJwSD68&libraries=places&callback=initMap"></script>

  
</head>
<body>
    <h1 class="text-center"><b>GEOLOCALIZACIÓN</b></h1>
    <div class="row">
        <div class="col-md-6 text-center">
          <center>
            <legend>LISTADO DE UBICACIONES</legend>
            <a href="<?php echo site_url('practicas/index');?>" class="btn btn-warning">
            <i class="glyphicon glyphicon-fast-backward"></i>
              Regresar          
          </a>
          <br>
          <br>
          </center>
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">CEDULA</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">APELLIDO</th>
                <th class="text-center">CELULAR</th>
                <th class="text-center">EMAIL</th>
                <th class="text-center">LATITUD_1</th>
                <th class="text-center">LONGITUD_1</th>
                <th class="text-center">LATITUD_2</th>
                <th class="text-center">LONGITUD_2</th>
                <th class="text-center">LATITUD_3</th>
                <th class="text-center">LONGITUD_3</th>
                <th class="text-center">LATITUD_4</th>
                <th class="text-center">LONGITUD_4</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($listado): ?>
                <?php foreach ($listado->result() as $listaTemporal): ?>
                  <tr>
                    <td class="text-center"><?php echo $listaTemporal->id_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->cedula_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->nombre_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->apellido_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->celular_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->email_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->latitud_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->longitud_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->latitud2_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->longitud2_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->latitud3_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->longitud3_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->latitud4_br; ?></td>
                    <td class="text-center"><?php echo $listaTemporal->longitud4_br; ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
        
        <div class="row">
          <div class="col-md-12 text-center">
            <legend>UBICACIONES GEORREFERENCIADAS</legend>
            <center>
              <h1 class="text-center"><i class="glyphicon glyphicon-map-marker"></i></h1>
              <div id="mapa" style="height:500px; width:90%;  border:2px solid black;"></div>
            </center>
          </div>
        </div>     
        <br>
        <script type="text/javascript">
          function initMap(){
              alert("API IMPORTADO EXITOSAMENTE");
            <?php if ($listado): ?>
              <?php foreach ($listado->result() as $listaTemporal): ?>
                var latitud_longitud = new google.maps.LatLng(<?php echo $listaTemporal->latitud_br; ?>,
                <?php echo $listaTemporal->longitud_br; ?>,
                <?php echo $listaTemporal->latitud2_br; ?>,
                <?php echo $listaTemporal->longitud2_br; ?>,
                <?php echo $listaTemporal->latitud3_br; ?>,
                <?php echo $listaTemporal->longitud3_br; ?>,
                <?php echo $listaTemporal->latitud4_br; ?>,
                <?php echo $listaTemporal->longitud4_br;  ?>
                )
                console.log(latitud_longitud);
                var mapa=new google.maps.Map(document.getElementById('mapa'),
                    {
                      center:latitud_longitud,
                      zoom:5,
                      mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                  );
              <?php endforeach; ?>
            <?php endif; ?>
          <?php if ($listado): ?>
              <?php foreach ($listado->result() as $fila): ?>
              var marcador = new google.maps.Marker({
              position: new google.maps.LatLng(<?php echo $fila->latitud_br; ?>, 
              <?php echo $listaTemporal->longitud_br; ?>,
              <?php echo $listaTemporal->latitud2_br; ?>,
              <?php echo $listaTemporal->longitud2_br; ?>,
              <?php echo $listaTemporal->latitud3_br; ?>,
              <?php echo $listaTemporal->longitud3_br; ?>,
              <?php echo $listaTemporal->latitud4_br; ?>,
              <?php echo $listaTemporal->longitud4_br; ?>
              ),
              map:mapa,
              title:"<?php echo $listaTemporal->id_br; ?>",
              });
              <?php endforeach; ?>
            <?php endif; ?>
            }
        </script>
      </div>    
  </body>
</html>