<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXV6fJ3AmkKcvYoxYWcAyQ5Y1ddJwSD68&libraries=places&callback=initMap" >
    </script>
    <meta charset="utf-8">
    <title>GEOLOCALIZACION</title>
  </head>
  <body>
    <div  style="background-color:GREY;" class="row">
      <h1 class="text-center">
        <i class="glyphicon glyphicon-globe"></i>
        <b style="color:white";>APP PRACTICA 2</b>
        <br>
        <h6 class="text-center" style="color:white";>DESAROLLADO POR: BARRAGAN LUIS & JAIRO REYES</h6>
      </h1>
    </div>
    <br>
    <form class="form-horizontal"  action="<?php echo site_url('practicas/guardar'); ?>"  method="post">
      <div class="row">
        <div class="col-md-1">
          <label for="">Cedula : </label>
        </div>
        <div class="col-md-4">
          <input type="number" required id="cedula_br" name="cedula_br" value="" class="form-control" placeholder="Ingrese su Cedula">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-1">
          <label for="">Apellido : </label>
        </div>
        <div class="col-md-4">
          <!-- el tipo de comentarios -->
          <input type="text" required id="apellido_br" name="apellido_br" value="" class="form-control" placeholder="Ingrese el Apellido">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-1">
          <label for="">Nombre : </label>
        </div>
        <div class="col-md-4">
          <!-- el tipo de comentarios -->
          <input type="text" required id="nombre_br" name="nombre_br" value="" class="form-control" placeholder="Ingrese sus Nombres">
        </div>
      </div>
      <br>

      <div class="row">
        <div class="col-md-4">
          <b>CELULAR :</b><br>
          <input type="number" required name="celular_br" class="form-control" placeholder="Ingrese numero celular" value="">
        </div>
        <div class="col-md-4">
          <b>EMAIL :</b><br>
          <input type="text" required name="email_br" class="form-control" placeholder="Ingrese email" value="">
        </div>
      </div>
      <br>
      <div class="row">
        <center><b><h4>COORDENADA_1</h4></b></center>
        <div class="col-md-5">
          <br>
          <br>
          <br>
          <br>
          <label for=""><b>LATITUD:</b></label>
          <input type="number" required name="latitud_br" id="latitud_br" class="form-control" placeholder="Ingrese la latitud" value="" readonly>
          <br>
          <label for=""><b>LONGITUD:</b></label>
          <input type="number" required name="longitud_br" id="longitud_br" class="form-control" placeholder="Ingrese la longitud" value="" readonly>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <br>
          <b><center><h4>MAPA__ #1</h4></center></b>
          <br>
          <div id="map" style="height:200px; width:100%; border:3px solid black;">
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
      <br>
      <div class="row">
        <center><b><h4>COORDENADA_2</h4></b></center>
        <div class="col-md-5">
          <br>
          <br>
          <br>
          <br>
          <label for=""><b>LATITUD:</b></label>
          <input type="number" required readonly name="latitud2_br" id="latitud2_br" class="form-control" placeholder="Ingrese la latitud" value="">
          <br>
          <label for=""><b>LONGITUD:</b></label>
          <input type="number" required readonly name="longitud2_br" id="longitud2_br" class="form-control" placeholder="Ingrese la longitud" value="">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <br>
          <b><center><h4>MAPA__ #2</h4></center></b>
          <br>
          <div id="mapa_2" style="height:200px; width:100%; border:3px solid black;">
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
      <br>
      <div class="row">
        <center><b><h4>COORDENADA_3</h4></b></center>
        <div class="col-md-5">
          <br>
          <br>
          <br>
          <br>
          <label for=""><b>LATITUD:</b></label>
          <input type="number" required readonly name="latitud3_br" id="latitud3_br" class="form-control" placeholder="Ingrese la latitud" value="">
          <br>
          <label for=""><b>LONGITUD:</b></label>
          <input type="number" required readonly name="longitud3_br" id="longitud3_br" class="form-control" placeholder="Ingrese la longitud" value="">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <br>
          <b><center><h4>MAPA__ #3</h4></center></b>
          <br>
          <div id="mapa_3" style="height:200px; width:100%; border:3px solid black;">
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
      <br>
      <div class="row">
        <center><b><h4>COORDENADA_4</h4></b></center>
        <div class="col-md-5">
          <br>
          <br>
          <br>
          <br>
          <label for=""><b>LATITUD:</b></label>
          <input type="number" required readonly name="latitud4_br" id="latitud4_br" class="form-control" placeholder="Ingrese la latitud" value="">
          <br>
          <label for=""><b>LONGITUD:</b></label>
          <input type="number" required readonly name="longitud4_br" id="longitud4_br" class="form-control" placeholder="Ingrese la longitud" value="">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <br>
          <b><center><h4>MAPA__ #4</h4></center></b>
          <br>
          <div id="mapa_4" style="height:200px; width:100%; border:3px solid black;">
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
      <br>

      <div class="col-md-4 text-center">
        <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;&nbsp;Guardar&nbsp;&nbsp;&nbsp; </button>
        &nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger"> &nbsp;&nbsp;&nbsp;Cancelar&nbsp;&nbsp;&nbsp; </button>
      </div>

      <br>
    </form>
    <script type="text/javascript">
      function initMap(){
          alert("API KEY IMPORTADO EXITOSAMENTE");
          //Definiendo una coordenada
          var latitud_longitud=new google.maps.LatLng(-0.9374805,-78.6161327);
          //creando el mapa
          var map=new google.maps.Map(document.getElementById('map'),
            {
              center:latitud_longitud,
              zoom:12,
              mapTypeId:google.maps.MapTypeId.ROADMAP
          });
              //Creando un marcador de mi domicilio
          var marcador = new google.maps.Marker({
            position: latitud_longitud,
            map:map,
            title:"SELECCIONE EL AREA DESEADA",
            draggable:true
            // icon:"https://static.thenounproject.com/png/5148-200.png"
          });

          google.maps.event.addListener(marcador,'dragend',function(event){
              var latitud=this.getPosition().lat();
              var longitud=this.getPosition().lng();
              //alert(latitud1+"\n"+longitud1);
              document.getElementById('latitud_br').value=latitud;
              document.getElementById('longitud_br').value=longitud;
            });


          var map=new google.maps.Map(document.getElementById('mapa_2'),
              {
                  center:latitud_longitud,
                  zoom:12,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              }
            );
            var marcador2=new google.maps.Marker({
              position:latitud_longitud,
              map:map,
              title:'SELECCIONE EL AREA DESEADA',
              draggable:true
            });

            google.maps.event.addListener(marcador2,'dragend',function(event){
              var latitud2=this.getPosition().lat();
              var longitud2=this.getPosition().lng();
            //  alert(latitud1+"\n"+longitud1)2;
            document.getElementById('latitud2_br').value=latitud2;
            document.getElementById('longitud2_br').value=longitud2;
            });

          var map=new google.maps.Map(document.getElementById('mapa_3'),
              {
                  center:latitud_longitud,
                  zoom:12,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              }
            );
            var marcador3=new google.maps.Marker({
              position:latitud_longitud,
              map:map,
              title:'SELECCIONE EL AREA DESEADA',
              draggable:true
            });

            google.maps.event.addListener(marcador3,'dragend',function(event){
              var latitud3=this.getPosition().lat();
              var longitud3=this.getPosition().lng();
            //  alert(latitud1+"\n"+longitud1)2;
            document.getElementById('latitud3_br').value=latitud3;
            document.getElementById('longitud3_br').value=longitud3;
          });

          var map=new google.maps.Map(document.getElementById('mapa_4'),
              {
                  center:latitud_longitud,
                  zoom:12,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              }
            );
            var marcador4=new google.maps.Marker({
              position:latitud_longitud,
              map:map,
              title:'SELECCIONE EL AREA DESEADA',
              draggable:true
            });

            google.maps.event.addListener(marcador4,'dragend',function(event){
              var latitud4=this.getPosition().lat();
              var longitud4=this.getPosition().lng();
            //  alert(latitud1+"\n"+longitud1)2;
            document.getElementById('latitud4_br').value=latitud4;
            document.getElementById('longitud4_br').value=longitud4;
          });
      }
    </script>
  </body>
</html>
